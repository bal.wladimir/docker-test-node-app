FROM node:16-alpine AS builder
WORKDIR /myapp
COPY . .
RUN npm install && npm run build

FROM nginx:1.22-alpine

COPY --from=builder /myapp/build /var/www/myapp
RUN ls -la /var/www
EXPOSE 80
ENTRYPOINT ["nginx", "-g", "daemon off;"]